# Jawaban UTS Praktikum Basis Data

# Nomor 1
Database Diagram
---
Perancangan database diagram traveloka berdasarkan permasalahan dunia nyata meliputi beberapa transaksi seperti pemesanan tiket pesawat dan booking hotel.

![Traveloka Diagram](Nomor 1/traveloka_database.png)

# Nomor 2
DDL (Data Definition Language)
---
Pembuatan tabel berdasarkan diagram pada nomor 1 dengan mengekspor DBML pada dbdiagram menjadi DDL MySQL

```sql
-- MySQL dump 10.13  Distrib 8.0.32, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: traveloka_database
-- ------------------------------------------------------
-- Server version	8.0.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `booking`
--

DROP TABLE IF EXISTS `booking`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `booking` (
  `id_booking` int NOT NULL AUTO_INCREMENT,
  `jenis_tiket` enum('Pesawat','Hotel','Kereta Api','Bus','Mobil') DEFAULT NULL,
  `id_tiket` int DEFAULT NULL,
  `id_user` int DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `status` int DEFAULT NULL,
  `qty` int DEFAULT NULL,
  PRIMARY KEY (`id_booking`),
  KEY `id_tiket` (`id_tiket`),
  KEY `id_user` (`id_user`),
  CONSTRAINT `booking_ibfk_1` FOREIGN KEY (`id_tiket`) REFERENCES `tiket_pesawat` (`id_tiket_pesawat`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `booking_ibfk_2` FOREIGN KEY (`id_tiket`) REFERENCES `hotel` (`id_hotel`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `booking_ibfk_3` FOREIGN KEY (`id_tiket`) REFERENCES `tiket_kereta_api` (`id_tiket_kereta_api`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `booking_ibfk_4` FOREIGN KEY (`id_tiket`) REFERENCES `tiket_bus` (`id_tiket_bus`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `booking_ibfk_5` FOREIGN KEY (`id_tiket`) REFERENCES `rental_mobil` (`id_rental_mobil`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `booking_ibfk_6` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hotel`
--

DROP TABLE IF EXISTS `hotel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hotel` (
  `id_hotel` int NOT NULL AUTO_INCREMENT,
  `nama_hotel` varchar(255) DEFAULT NULL,
  `kota` varchar(255) DEFAULT NULL,
  `fasilitas` varchar(255) DEFAULT NULL,
  `harga` int DEFAULT NULL,
  `rating` decimal(2,1) DEFAULT NULL,
  PRIMARY KEY (`id_hotel`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `maskapai`
--

DROP TABLE IF EXISTS `maskapai`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `maskapai` (
  `id_maskapai` int NOT NULL AUTO_INCREMENT,
  `nama_maskapai` varchar(255) DEFAULT NULL,
  `kode_IATA` varchar(255) DEFAULT NULL,
  `jenis_pesawat` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_maskapai`),
  CONSTRAINT `maskapai_ibfk_1` FOREIGN KEY (`id_maskapai`) REFERENCES `tiket_pesawat` (`id_tiket_pesawat`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `metode_pembayaran`
--

DROP TABLE IF EXISTS `metode_pembayaran`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `metode_pembayaran` (
  `id_metode` int NOT NULL AUTO_INCREMENT,
  `metode_pembayaran` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_metode`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `partner`
--

DROP TABLE IF EXISTS `partner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `partner` (
  `id_mitra` int NOT NULL AUTO_INCREMENT,
  `nama_mitra` varchar(255) DEFAULT NULL,
  `jenis_mitra` varchar(255) DEFAULT NULL,
  `nomor_telepon_mitra` varchar(255) DEFAULT NULL,
  `alamat_mitra` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_mitra`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `promo`
--

DROP TABLE IF EXISTS `promo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `promo` (
  `id_promo` int NOT NULL AUTO_INCREMENT,
  `kode_promo` int DEFAULT NULL,
  `diskon` int DEFAULT NULL,
  `tanggal_berlaku_promo` int DEFAULT NULL,
  PRIMARY KEY (`id_promo`),
  CONSTRAINT `promo_ibfk_1` FOREIGN KEY (`id_promo`) REFERENCES `tiket_kereta_api` (`id_tiket_kereta_api`),
  CONSTRAINT `promo_ibfk_2` FOREIGN KEY (`id_promo`) REFERENCES `tiket_pesawat` (`id_tiket_pesawat`),
  CONSTRAINT `promo_ibfk_3` FOREIGN KEY (`id_promo`) REFERENCES `tiket_bus` (`id_tiket_bus`),
  CONSTRAINT `promo_ibfk_6` FOREIGN KEY (`id_promo`) REFERENCES `rental_mobil` (`id_rental_mobil`),
  CONSTRAINT `promo_ibfk_7` FOREIGN KEY (`id_promo`) REFERENCES `tiket_kereta_api` (`id_tiket_kereta_api`),
  CONSTRAINT `promo_ibfk_9` FOREIGN KEY (`id_promo`) REFERENCES `hotel` (`id_hotel`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rental_mobil`
--

DROP TABLE IF EXISTS `rental_mobil`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rental_mobil` (
  `id_rental_mobil` int NOT NULL AUTO_INCREMENT,
  `penyedia_rental` varchar(255) DEFAULT NULL,
  `alamat_rental` varchar(255) DEFAULT NULL,
  `telepon_rental` varchar(255) DEFAULT NULL,
  `harga` int DEFAULT NULL,
  `tipe_kendaraan` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_rental_mobil`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `review`
--

DROP TABLE IF EXISTS `review`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `review` (
  `id_review` int NOT NULL AUTO_INCREMENT,
  `nama_pengguna` varchar(255) DEFAULT NULL,
  `komentar` varchar(255) DEFAULT NULL,
  `rating` int DEFAULT NULL,
  PRIMARY KEY (`id_review`),
  CONSTRAINT `review_ibfk_1` FOREIGN KEY (`id_review`) REFERENCES `user` (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `status`
--

DROP TABLE IF EXISTS `status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `status` (
  `id_status` int NOT NULL AUTO_INCREMENT,
  `nama_status` varchar(50) NOT NULL,
  `deskripsi` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_status`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tiket_bus`
--

DROP TABLE IF EXISTS `tiket_bus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tiket_bus` (
  `id_tiket_bus` int NOT NULL AUTO_INCREMENT,
  `nama_po` varchar(255) DEFAULT NULL,
  `kelas` varchar(255) DEFAULT NULL,
  `harga` int DEFAULT NULL,
  `asal` varchar(255) DEFAULT NULL,
  `tujuan` varchar(255) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  PRIMARY KEY (`id_tiket_bus`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tiket_kereta_api`
--

DROP TABLE IF EXISTS `tiket_kereta_api`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tiket_kereta_api` (
  `id_tiket_kereta_api` int NOT NULL AUTO_INCREMENT,
  `nama_kereta` varchar(255) DEFAULT NULL,
  `asal` varchar(255) DEFAULT NULL,
  `tujuan` varchar(255) DEFAULT NULL,
  `tanggal_berangkat` date DEFAULT NULL,
  `tanggal_pulang` date DEFAULT NULL,
  `harga` int DEFAULT NULL,
  `kelas` varchar(255) DEFAULT NULL,
  `waktu_keberangkatan` time DEFAULT NULL,
  `waktu_kedatangan` time DEFAULT NULL,
  `lama_perjalanan` time DEFAULT NULL,
  PRIMARY KEY (`id_tiket_kereta_api`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tiket_pesawat`
--

DROP TABLE IF EXISTS `tiket_pesawat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tiket_pesawat` (
  `id_tiket_pesawat` int NOT NULL AUTO_INCREMENT,
  `nomor_penerbangan` varchar(255) DEFAULT NULL,
  `nama_maskapai` varchar(255) DEFAULT NULL,
  `tanggal_keberangkatan` date DEFAULT NULL,
  `tanggal_kedatangan` date DEFAULT NULL,
  `waktu_keberangkatan` time DEFAULT NULL,
  `waktu_kedatangan` time DEFAULT NULL,
  `asal` varchar(255) DEFAULT NULL,
  `tujuan` varchar(255) DEFAULT NULL,
  `kelas_penerbangan` varchar(255) DEFAULT NULL,
  `harga` int DEFAULT NULL,
  `lama_perjalanan` varchar(255) DEFAULT NULL,
  `id_maskapai` int DEFAULT NULL,
  PRIMARY KEY (`id_tiket_pesawat`),
  KEY `fk_tiket_pesawat_maskapai` (`id_maskapai`),
  KEY `idx_nama_maskapai` (`nama_maskapai`),
  CONSTRAINT `fk_tiket_pesawat_maskapai` FOREIGN KEY (`id_maskapai`) REFERENCES `maskapai` (`id_maskapai`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `transaksi`
--

DROP TABLE IF EXISTS `transaksi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `transaksi` (
  `id_transaksi` int NOT NULL AUTO_INCREMENT,
  `id_booking` int DEFAULT NULL,
  `metode_pembayaran` int DEFAULT NULL,
  `tanggal_pembayaran` date DEFAULT NULL,
  `jumlah_pembayaran` decimal(10,2) DEFAULT NULL,
  `status` int DEFAULT NULL,
  PRIMARY KEY (`id_transaksi`),
  KEY `metode_pembayaran` (`metode_pembayaran`),
  KEY `status` (`status`),
  KEY `id_booking` (`id_booking`),
  CONSTRAINT `transaksi_ibfk_1` FOREIGN KEY (`metode_pembayaran`) REFERENCES `metode_pembayaran` (`id_metode`),
  CONSTRAINT `transaksi_ibfk_2` FOREIGN KEY (`status`) REFERENCES `status` (`id_status`),
  CONSTRAINT `transaksi_ibfk_3` FOREIGN KEY (`id_booking`) REFERENCES `booking` (`id_booking`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'IGNORE_SPACE,ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `update_booking_status` AFTER INSERT ON `transaksi` FOR EACH ROW BEGIN

    DECLARE total_pembayaran DECIMAL(10, 2);
    SET total_pembayaran = (SELECT SUM(jumlah_pembayaran) FROM transaksi WHERE id_booking = NEW.id_booking);
    IF total_pembayaran != 0 THEN

        UPDATE booking

        SET status = 2

        WHERE id_booking = NEW.id_booking;
    ELSE

        UPDATE booking

        SET status = 1

        WHERE id_booking = NEW.id_booking;
    END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `id_user` int NOT NULL AUTO_INCREMENT,
  `nama_lengkap` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `kelamin` enum('L','P') DEFAULT NULL,
  `nomor_telepon` varchar(255) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `kota_tinggal` varchar(255) DEFAULT NULL,
  `kecamatan` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_user`),
  UNIQUE KEY `email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-07-08 12:39:04

```

# Nomor 3
DML (Data Manipulation Language)
---
Serangkaian perintah untuk memanipulasi data yang ada di Database

| NO |  Use Case |
| ------ | ----- |
| 1 | Menambahkan Data/Insert Ke tabel|     | 2 | Membaca Isi data tabel |
| 3 | Mengupdate isi data tabel |
| 4 | Menghapus isi data tabel  |
| 5 | User Bisa Pesan Tiket Pesawat   |
| 6 | User Bisa Pesan Hotel      |
| 7 | User Bisa Pesan Tiket Kereta Api |
| 8 | User Bisa Pesan Tiket Bus |
| 9 | User Bisa Mendapatkan Promo/diskon Terhadap pemesanan tiket |
| 10 | User bisa memberikan ulasan |
| 11 | User bisa memberikan rating |
| 12 | User bisa membatalkan pesanan |
| 13 | User melakukan pencarian tiket pesawat, kereta api, dan hotel |
| 14 | User melakukan pembayaran atas pemesanan tiket yang dipilih |
| 15 | User dapat melihat ulasan informasi t destinasi wisata tertentu |
| 16 | User dapat melihat riwayat tiket yang dipesan |
| 17 | User dapat melakukan pengaturan informasi akun |
| 18 | User dapat menghubungi layanan pelanggan untuk mendapatkan bantuan atau informasi lebih lanjut |
| 19 | User dapat mengganti jadwal pemesanan tiket tertentu |
| 20 | User dapat mengakses informasi tentang fasilitas dan layanan tambahan yang tersedia di hotel atau maskapai tertentu, seperti makanan khusus atau akses ke lounge VIP |
| 21 | User dapat mengunduh atau mencetak tiket atau dokumen penting terkait perjalanan atau penginapan yang sudah dipesan |
| 22 | User dapat melakukan perbandingan antara harga atau fasilitas yang ditawarkan oleh beberapa maskapai atau hotel berbeda, sehingga dapat memilih opsi yang paling sesuai dengan kebutuhan dan anggaran |
| 23 | User dapat Mengikuti program loyalty atau member untuk mendapatkan potongan harga atau keuntungan lain ketika melakukan pemesanan di Traveloka |
| 24 | User dapat memilih tempat duduk atau kamar hotel yang diinginkan, jika tersedia opsi untuk memilih sendiri |
| 25 | User dapat memilih tujuan destinati |
| 26 | User dapat memilih tipe kamar |
| 27 | User dapat melihat rentang harga tiket dari termurah dan termahal |
| 28 | Direktur bisa melihat preferensi pelanggan |

INSERT Data User
---
```sql
INSERT INTO user (nama_lengkap,nama_pengguna,nomor_telepon,kelamin,tanggal_lahir,kota_tinggal,alamat,email,password,akun_terhubung)
VALUES ('Yuda Ristian Asgari','yudaristian22',0881023764187,'L',20030322,'Bandung','Rancaekek','yudaramasenju@gmail.com','yuda123','Google'),
('Siti Arifah Sumiyati','sitiarifah',088657340846,'p',20030211,'SUmedang','Jatinangor','sitiarifah@gmail.com','sitiarifah123','Google'),
('Sopian Renaldi','boopablo',0887313620967,'L',20020910,'Bandung','Rancaekek','sopianrenaldi@gmail.com','sopian123','Facebook'),
('Ridwan Ahmad Fauzan','ridwanahmad',088408172898,'L',20030510,'Subang','Cibiru','ridwanahmad@gmail.com','ridwan123','Google'),
('Helmi Luthfi Januar','gatau123',088336534217,'L',20030102,'Bandung','Rancaekek','helmiluthfi@gmail.com','helmi123','Facebook'),
('Agus Aji Pangestu','agusajialpra',088591004758,'L',20020810,'Bandung','Cibiru','agusaji@gmail.com','agusaji123','Facebook'),
('Ilmi Septyasella','cutiespetit',0884791709142,'P',20020908,'Sumedang','Parakan Muncang','ilmiseptya@gmail.com','ilmi123','Facebook'),
('Rahma Aulia Febrianti','rahmaaulf',0882806236738,'P',20030209,'Bandung','Cileunyi','rahmaaulia@gmail.com','rahma123','Google'),
('Muhammad Zaky','muhammadzaky',088489489859,'L',20030125,'Bandung','Banjaran','zakyms@gmail.com','zaky123','Google'),
('Sami Irhamnillah','samiirham',0886388461215,'L',20030203,'Indramayu','Buah Batu','samiirham@gmail.com','sami123','Google'),
('Bayu Sapta Syawali','by.u',08867585857,'L',20020915,'Bandung','Rancaekek','bayusapta@gmail.com','bayu123','Google'),
('Sri Purnama Dewi','sriprnmdw',08816964565,'P',20040505,'Bandung','Cileunyi','sripurnama@gmail.com','sri123','Facebook'),
('Alissa Qotrunnada','alissaq',0881343906230,'P',20031202,'Bandung','Rancaekek','alissaq@gmail.com','alissa123','Google'),
('Wiki Nurrohman','wikinur',088639674585,'L',20011125,'Subang','Kiara Condong','wikinur@gmail.com','wiki123','Google'),
('Muhammad Syamil','syamil',088281874287,'L',20031012,'Ciamis','Cibiru','msyamil@gmail.com','syamil123','Google'),
('Raihan Firdaus','daus','0881023766198','L','20021201','Bandung','Cipadung','raihanfirdaus@gmail.com','raihan123','Facebook'),
('Tifani Apriliani','tifaaa','088102789382','P','20020302','Subang','Cibiru','tifaniapriliani@gmail.com','tifani123','Facebook'),
('Gisva Rapa Pradila','agis','0881022892189','P','20021231','Bandung','Rancaekek','gisvarapa@gmail.com','gisva123','Facebook'),
('Mutiara Annisa','utii','0882098645178','P','20030409','Bandung','Cileunyi','mutiara@gmail.com','mutiara123','Google'),
('Zaskiya','jaskeu','0889208901928','P','20030910','Sumedang','Jatinangor','zaskiya@gmail.com','zaskiya123','Google')
```
INSERT Data Hotel
---
```sql
INSERT INTO hotel(nama_hotel, lokasi, fasilitas,harga,rating)
values('Grand Tjokro Premiere Bandung', 'Bandung','AC + Restoran + Kolam renang + Parkir + Lift + Wifi', 584100,8.5),
('The Alana Yogyakarta Hotel & Convention Center', 'Yogyakarta','AC + Restoran + Kolam renang + Parkir + Lift + Wifi', 710000,8.8),
('Melia Purosani Yogyakarta', 'Yogyakarta','AC + Restoran + Kolam renang + Parkir + Lift + Wifi', 1350000,8.7),
('ARTOTEL Suites Mangkuluhur Jakarta', 'Jakarta','AC + Restoran + Kolam renang + Parkir + Lift + Wifi', 963000,8.4),
('Hotel Indonesia Kempinski Jakarta', 'Jakarta','AC + Restoran + Kolam renang + Parkir + Lift + Wifi', 3600100,8.9),
('Grand Darmo Suite by AMITHYA', 'Surabaya','AC + Restoran + Kolam renang + Parkir + Lift + Wifi', 400000,8.3),
('The Apurva Kempinski Bali', 'Bali','AC + Restoran + Kolam renang + Parkir + Lift + Wifi', 5600000,8.9),
('The Anvaya Beach Resort Bali', 'Bali','AC + Restoran + Kolam renang + Parkir + Lift + Wifi', 2900000,8.9),
('Lorin Sentul Hotel', 'Bogor','AC + Restoran + Kolam renang + Parkir + Lift + Wifi', 427000,7.9),
('Salak Hevea Syariah', 'Bogor','AC + Restoran + Kolam renang + Parkir + Lift + Wifi', 300000,8.4)
```
INSERT Data Tiket Pesawat
---
```sql
INSERT INTO maskapai (nama_maskapai, kode_IATA, jenis_pesawat, tujuan)
values ('Air Asia','QZ','Boeing 737','Surabaya'),
('Batik Air','ID','Boeing 737','Surabaya'),
('Citilink','QG','Boeing 737','Surabaya'),
('Garuda Indonesia','GA','Boeing 737','Surabaya'),
('Lion Air','JT','Boeing 737','Surabaya'),
('SuperAirJet','SJ','Boeing 737','Surabaya'),
('Wings Air','IW','Boeing 737','Surabaya'),
('Air Asia','QZ','Boeing 747','Surabaya'),
('Batik Air','ID','Boeing 747','Surabaya'),
('Citilink','QG','Boeing 747','Surabaya'),
('Garuda Indonesia','GA','Boeing 747','Surabaya'),
('Lion Air','JT','Boeing 747','Surabaya'),
('SuperAirJet','SJ','Boeing 747','Surabaya'),
('Wings Air','IW','Boeing 747','Surabaya'),
('Air Asia','QZ','Boeing 737','Yogyakarta'),
('Batik Air','ID','Boeing 737','Yogyakarta'),
('Citilink','QG','Boeing 737','Yogyakarta'),
('Garuda Indonesia','GA','Boeing 737','Yogyakarta'),
('Lion Air','JT','Boeing 737','Yogyakarta'),
('SuperAirJet','SJ','Boeing 737','Yogyakarta'),
('Wings Air','IW','Boeing 737','Yogyakarta'),
('Air Asia','QZ','Boeing 747','Yogyakarta'),
('Batik Air','ID','Boeing 747','Yogyakarta'),
('Citilink','QG','Boeing 747','Yogyakarta'),
('Garuda Indonesia','GA','Boeing 747','Yogyakarta'),
('Lion Air','JT','Boeing 747','Yogyakarta'),
('SuperAirJet','SJ','Boeing 747','Yogyakarta'),
('Wings Air','IW','Boeing 747','Yogyakarta')
```
READ
---
Membaca isi data yang ada pada tabel
```sql
READ
SELECT * FROM USER;
SELECT * FROM USER WHERE kelamin = 'L';
```
UPDATE
---
Melakukan perubahan data
```sql
UPDATE
UPDATE user
SET nama_pengguna = 'fani'
WHERE id_user = 17;
```

DELETE
---
Melakukan Penghapusan isi data
```sq
DELETE
DELETE FROM user
WHERE id_user = 19;
```

# Nomor 4 
DQL (Data Query Language)
---
Digunakan untuk melakukan query atau pengambilan data dari basis data tanpa melakukan perubahan pada data itu sendiri.

### 📊 Analisis Pertanyaan

| No | Pertanyaan |
| ------ | ------ |
|     1   | Bagaimana Menampilkan Banyaknya User berdasarkan gender?       |
|      2  | Bagaimana Menampilkan banyaknya gender berdasarkan alamat?       |
| 3 | Bagaimana menampilkan total tiket yang tersedia berdasarkan nama nama maskapai? |
| 4 | Bagaiamana menampilkan harga termahal dari tiket pesawat berdasarkan tujuan? |
| 5 | Bagaiamana menampilkan harga termurah dari tiket pesawat berdasarkan tujuan? |
| 6 | Bagaiamana menampilkan harga rata rata dari tiket pesawat berdasarkan tujuan? |
| 7 | Bagaimana menampilkan 2 tabel sekaligus? |

### 1. Bagaimana menampilkan Banyaknya User berdasarkan gender?
```sql
select 
  count(*) as total_user,
  kelamin
from user
group by kelamin;
```

### 2. Bagaimana menampilkan banyaknya gender berdasarkan alamat?
```sql
select 
  alamat,
  kelamin,
  count(*) as total
from user
group by alamat,kelamin
order by kelamin,alamat
```

### 3. Bagaimana menampilkan total tiket yang tersedia berdasarkan nama nama maskapai?
```sql
select 
  count(id_tiket_pesawat) as 'total_tiket',
  nama_maskapai
from tiket_pesawat
group by nama_maskapai
order by nama_maskapai
```

### 4. Bagaiamana menampilkan harga termahal dari tiket pesawat berdasarkan tujuan?
```sql
select 
  max(harga) as 'Harga Termahal',
  tujuan
from tiket_pesawat 
group by tujuan 
order by tujuan asc
```

### 5. Bagaiamana menampilkan harga termurah dari tiket pesawat berdasarkan tujuan
```sql
select 
  min(harga) as 'Harga Termurah',
  tujuan 
from tiket_pesawat 
group by tujuan 
order by tujuan asc
```

### 6. Bagaiamana menampilkan harga rata rata dari tiket pesawat berdasarkan tujuan?
```sql
select 
  avg(harga) as 'Harga Termurah',
  tujuan
from tiket_pesawat 
group by tujuan 
order by tujuan asc
```

### 7. Bagaimana menampilkan 2 tabel sekaligus?
```sql
select 
  tp.id_tiket_pesawat,
  tp.nama_maskapai,
  tp.tujuan,
  maskapai.id_maskapai,
  maskapai.kode_IATA,
  maskapai.nama_maskapai,
  maskapai.jenis_pesawat
from tiket_pesawat as tp
join maskapai on (tp.id_tiket_pesawat = maskapai.id_maskapai);
```

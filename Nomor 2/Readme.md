# Source Code DDL

<pre><code>CREATE TABLE `user` (
  `id_user` int auto_increment primary KEY,
  `nama_lengkap` varchar(255),
  `nama_pengguna` varchar(255),
  `nomor_telepon` varchar(255),
  `kelamin` enum('L','P'),
  `tanggal_lahir` date,
  `kota_tinggal` varchar(255),
  `alamat` varchar(255),
  `email` varchar(255),
  `password` varchar(255),
  `akun_terhubung` varchar(255)
);

CREATE TABLE `tiket_pesawat` (
  `id_tiket_pesawat` int auto_increment PRIMARY KEY,
  `nomor_penerbangan` int,
  `maskapai_penerbangan` varchar(255),
  `tanggal_keberangkatan` date,
  `tanggal_kedatangan` date,
  `waktu_keberangkatan` time,
  `waktu_kedatangan` time,
  `asal` varchar(255),
  `tujuan` varchar(255),
  `kelas_penerbangan` varchar(255),
  `Harga` int,
  `jumlah_kursi_yang_tersedia` int,
  `jumlah_penumpang` int,
  `lama_perjalanan` time
);

CREATE TABLE `pesawat_hotel` (
  `id_pesawat_hotel` int auto_increment PRIMARY KEY,
  `nomor_penerbangan` int,
  `maskapai_penerbangan` varchar(255),
  `tanggal_keberangkatan` date,
  `tanggal_kedatangan` date,
  `waktu_keberangkatan` time,
  `waktu_kedatangan` time,
  `asal` varchar(255),
  `tujuan` varchar(255),
  `kelas_penerbangan` varchar(255),
  `harga` int,
  `jumlah_kursi_yang_tersedia` int,
  `jumlah_penumpang` int,
  `lama_perjalanan` int,
  `kota_tujuan_menginap` varchar(255),
  `checkin` datetime,
  `checkout` datetime,
  `jumlah_tamu` int,
  `jumlah_kamar` int
);

CREATE TABLE `tiket_kereta_api` (
  `id_tiket_kereta_api` auto_increment int PRIMARY KEY,
  `no_pesanan` int,
  `asal` varchar(255),
  `tujuan` varchar(255),
  `tanggal_berangkat` date,
  `tanggal_pulang` date,
  `jumlah_penumpang` int,
  `jumlah_kursi_yang_tersedia` int,
  `harga` int,
  `waktu_keberangkatan` time,
  `waktu_kedatangan` time,
  `lama_perjalanan` time,
  `nomor_kursi` int
);

CREATE TABLE `tiket_bus_shuttle` (
  `id_tiket_bus_shuttle` int auto_increment PRIMARY KEY,
  `harga` int,
  `asal` varchar(255),
  `tujuan` varchar(255),
  `tanggal_berangkat` date,
  `tanggal_pulang` date,
  `jumlah_penumpang` varchar(255),
  `nomor_kursi` int,
  `jumlah_kursi_yang_tersedia` int
);

CREATE TABLE `antar_jemput_bandara` (
  `id_antar_jemput_bandara` int auto_increment PRIMARY KEY,
  `nomor_penerbangan` int,
  `maskapai_penerbangan` int,
  `asal` varchar(255),
  `tujuan` varchar(255),
  `harga` int,
  `tanggal_berangkat` date,
  `tanggal_kedatangan` date,
  `waktu_keberangkatan` time,
  `jumlah_penumpang` int,
  `kapasitas_penumpang` int,
  `tipe_kendaraan` varchar(255),
  `warna_kendaraan` varchar(255),
  `detil_kendaraan` varchar(255)
);

CREATE TABLE `rental_mobil` (
  `id_rental_mobil` int auto_increment PRIMARY KEY,
  `harga` int,
  `lokasi_rental` varchar(255),
  `tanggal_mulai_rental` date,
  `tanggal_selesai_rental` date,
  `waktu_mulai` time,
  `waktu_selesai` time,
  `tipe_kendaraan` varchar(255),
  `kapasitas_penumpang` int,
  `jumlah_penumpang` int,
  `penyedia_rental` varchar(255)
);

CREATE TABLE `hotel` (
  `id_hotel` int auto_increment PRIMARY KEY,
  `nama_hotel` varchar(255),
  `lokasi` varchar(255),
  `jenis_kamar` varchar(255),
  `fasilitas` varchar(255),
  `harga` int,
  `jumlah_kamar_yang_tersedia` int
);

CREATE TABLE `holiday_stays` (
  `id_holiday_stays` int auto_increment PRIMARY KEY,
  `nama_hotel` varchar(255),
  `harga` int,
  `lokasi` varchar(255),
  `tujuan` varchar(255),
  `checkin` datetime,
  `jumlah_tamu` int,
  `jenis_kamar` varchar(255),
  `fasilitas` varchar(255),
  `jumlah_kamar_yang_tersedia` int
);

CREATE TABLE `xperience` (
  `id_xperience` int auto_increment PRIMARY KEY,
  `nama_tempat` varchar(255),
  `harga` int,
  `lokasi` varchar(255),
  `detil_produk` varchar(255),
  `fasilitas` varchar(255)
);

CREATE TABLE `pemesanan` (
  `id_pemesanan` int auto_increment PRIMARY KEY,
  `nama_lengkap` varchar(255),
  `nomor_telepon` int,
  `email` varchar(255),
  `titel` varchar(255)
);

CREATE TABLE `transaksi` (
  `id_transaksi` int auto_increment PRIMARY KEY,
  `nomor_transaksi` int,
  `tanggal_transaksi` datetime,
  `jumlah_pembayaran` int,
  `status_pembayaran` varchar(255),
  `metode_pembayaran` varchar(255)
);

CREATE TABLE `promo` (
  `id_promo` int auto_increment PRIMARY KEY,
  `kode_promo` int,
  `diskon` int,
  `tanggal_berlaku_promo` int
);

CREATE TABLE `review` (
  `id_review` int auto_increment PRIMARY KEY,
  `nama_pengguna` varchar(255),
  `komentar` varchar(255),
  `rating` int
);

CREATE TABLE `partner` (
  `id_partner` int auto_increment PRIMARY KEY,
  `nama_partner` varchar(255),
  `jeni_partner` varchar(255),
  `alamat` varchar(255),
  `nomor_telepon` varchar(255),
  `status` varchar(255)
);

CREATE TABLE `maskapai` (
  `id_maskapai` int auto_increment PRIMARY KEY,
  `nama_maskapai` varchar(255),
  `kode_maskapai` int,
  `jenis_pesawat` varchar(255)
);

CREATE TABLE `kamar_hotel` (
  `id_kamar` int auto_increment PRIMARY KEY,
  `jenis` varchar(255),
  `fasilitas` varchar(255),
  `harga` int,
  `info_tamu` varchar(255),
  `deskripsi` varchar(255)
);

ALTER TABLE `pemesanan` ADD FOREIGN KEY (`id_pemesanan`) REFERENCES `user` (`id_user`);

ALTER TABLE `transaksi` ADD FOREIGN KEY (`id_transaksi`) REFERENCES `pemesanan` (`id_pemesanan`);

ALTER TABLE `review` ADD FOREIGN KEY (`id_review`) REFERENCES `user` (`id_user`);

ALTER TABLE `pemesanan` ADD FOREIGN KEY (`id_pemesanan`) REFERENCES `tiket_pesawat` (`id_tiket_pesawat`);

ALTER TABLE `pemesanan` ADD FOREIGN KEY (`id_pemesanan`) REFERENCES `pesawat_hotel` (`id_pesawat_hotel`);

ALTER TABLE `pemesanan` ADD FOREIGN KEY (`id_pemesanan`) REFERENCES `tiket_kereta_api` (`id_tiket_kereta_api`);

ALTER TABLE `pemesanan` ADD FOREIGN KEY (`id_pemesanan`) REFERENCES `tiket_bus_shuttle` (`id_tiket_bus_shuttle`);

ALTER TABLE `pemesanan` ADD FOREIGN KEY (`id_pemesanan`) REFERENCES `antar_jemput_bandara` (`id_antar_jemput_bandara`);

ALTER TABLE `pemesanan` ADD FOREIGN KEY (`id_pemesanan`) REFERENCES `rental_mobil` (`id_rental_mobil`);

ALTER TABLE `pemesanan` ADD FOREIGN KEY (`id_pemesanan`) REFERENCES `hotel` (`id_hotel`);

ALTER TABLE `holiday_stays` ADD FOREIGN KEY (`id_holiday_stays`) REFERENCES `pemesanan` (`id_pemesanan`);

ALTER TABLE `pemesanan` ADD FOREIGN KEY (`id_pemesanan`) REFERENCES `xperience` (`id_xperience`);

ALTER TABLE `review` ADD FOREIGN KEY (`id_review`) REFERENCES `transaksi` (`id_transaksi`);

ALTER TABLE `promo` ADD FOREIGN KEY (`id_promo`) REFERENCES `tiket_pesawat` (`id_tiket_pesawat`);

ALTER TABLE `promo` ADD FOREIGN KEY (`id_promo`) REFERENCES `tiket_bus_shuttle` (`id_tiket_bus_shuttle`);

ALTER TABLE `promo` ADD FOREIGN KEY (`id_promo`) REFERENCES `pesawat_hotel` (`id_pesawat_hotel`);

ALTER TABLE `promo` ADD FOREIGN KEY (`id_promo`) REFERENCES `antar_jemput_bandara` (`id_antar_jemput_bandara`);

ALTER TABLE `promo` ADD FOREIGN KEY (`id_promo`) REFERENCES `rental_mobil` (`id_rental_mobil`);

ALTER TABLE `promo` ADD FOREIGN KEY (`id_promo`) REFERENCES `tiket_kereta_api` (`id_tiket_kereta_api`);

ALTER TABLE `promo` ADD FOREIGN KEY (`id_promo`) REFERENCES `holiday_stays` (`id_holiday_stays`);

ALTER TABLE `promo` ADD FOREIGN KEY (`id_promo`) REFERENCES `hotel` (`id_hotel`);

ALTER TABLE `maskapai` ADD FOREIGN KEY (`id_maskapai`) REFERENCES `partner` (`id_partner`);

ALTER TABLE `review` ADD FOREIGN KEY (`id_review`) REFERENCES `tiket_pesawat` (`id_tiket_pesawat`);

ALTER TABLE `review` ADD FOREIGN KEY (`id_review`) REFERENCES `xperience` (`id_xperience`);

ALTER TABLE `review` ADD FOREIGN KEY (`id_review`) REFERENCES `hotel` (`id_hotel`);

ALTER TABLE `review` ADD FOREIGN KEY (`id_review`) REFERENCES `pesawat_hotel` (`id_pesawat_hotel`);

ALTER TABLE `review` ADD FOREIGN KEY (`id_review`) REFERENCES `tiket_bus_shuttle` (`id_tiket_bus_shuttle`);

ALTER TABLE `review` ADD FOREIGN KEY (`id_review`) REFERENCES `tiket_kereta_api` (`id_tiket_kereta_api`);

ALTER TABLE `review` ADD FOREIGN KEY (`id_review`) REFERENCES `antar_jemput_bandara` (`id_antar_jemput_bandara`);

ALTER TABLE `review` ADD FOREIGN KEY (`id_review`) REFERENCES `rental_mobil` (`id_rental_mobil`);

ALTER TABLE `review` ADD FOREIGN KEY (`id_review`) REFERENCES `holiday_stays` (`id_holiday_stays`);

ALTER TABLE `hotel` ADD FOREIGN KEY (`id_hotel`) REFERENCES `pesawat_hotel` (`id_pesawat_hotel`);

ALTER TABLE `tiket_pesawat` ADD FOREIGN KEY (`id_tiket_pesawat`) REFERENCES `pesawat_hotel` (`id_pesawat_hotel`);

ALTER TABLE `hotel` ADD FOREIGN KEY (`id_hotel`) REFERENCES `kamar_hotel` (`id_kamar`);
</pre></code>

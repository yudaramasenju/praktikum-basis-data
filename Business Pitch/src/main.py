from sqlalchemy import create_engine, Column, Integer, String, Enum, Date
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from fastapi import FastAPI, HTTPException
from datetime import date
from pydantic import BaseModel
from enums import JenisKelamin
from typing import List

# Buat koneksi ke database MySQL
DATABASE_URL = "mysql+pymysql://root:manchester7@localhost:3306/traveloka_database"
engine = create_engine(DATABASE_URL)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()

class User(Base):
    __tablename__ = "user"
    id_user = Column(Integer, primary_key=True)
    nama_lengkap = Column(String(255))
    username = Column(String(255))
    password = Column(String(255))
    email = Column(String(255), unique=True)
    kelamin = Column(Enum(JenisKelamin), default=JenisKelamin.L)
    nomor_telepon = Column(String(255))
    tanggal_lahir = Column(Date)  # Menggunakan tipe Date
    kota_tinggal = Column(String(255))
    kecamatan = Column(String(255))

# Buat Tabel Jika Belum Ada
Base.metadata.create_all(bind=engine)

class UserCreate(BaseModel):
    nama_lengkap: str
    username: str
    password: str
    email: str
    kelamin: JenisKelamin
    nomor_telepon: str
    tanggal_lahir: date  # Menggunakan tipe date
    kota_tinggal: str
    kecamatan: str

    class Config:
        orm_mode = True

class UserUpdate(BaseModel):
    nama_lengkap: str = None
    username: str = None
    password: str = None
    email: str = None
    kelamin: JenisKelamin = None
    nomor_telepon: str = None
    tanggal_lahir: date = None  # Menggunakan tipe date
    kota_tinggal: str = None
    kecamatan: str = None

class UserOut(BaseModel):
    id_user: int
    nama_lengkap: str
    username: str
    email: str
    kelamin: JenisKelamin
    nomor_telepon: str
    tanggal_lahir: date  # Menggunakan tipe date
    kota_tinggal: str

# Endpoint
app = FastAPI()

@app.post("/user", response_model=UserOut)
def create_user(user: UserCreate):
    db = SessionLocal()
    db_user = User(
        nama_lengkap=user.nama_lengkap,
        username=user.username,
        password=user.password,
        email=user.email,
        kelamin=user.kelamin,
        nomor_telepon=user.nomor_telepon,
        tanggal_lahir=user.tanggal_lahir,
        kota_tinggal=user.kota_tinggal,
        kecamatan=user.kecamatan
    )
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user


@app.get("/user", response_model=List[UserOut])
def read_users():
    db = SessionLocal()
    users = db.query(User).all()
    return [UserOut(
        id_user=user.id_user,
        nama_lengkap=user.nama_lengkap,
        username=user.username,
        email=user.email,
        kelamin=user.kelamin,
        nomor_telepon=user.nomor_telepon,
        tanggal_lahir=user.tanggal_lahir,
        kota_tinggal=user.kota_tinggal,
        kecamatan=user.kecamatan
    ) for user in users]

@app.get("/user/{user_id}", response_model=UserOut)
def read_user(user_id: int):
    db = SessionLocal()
    user = db.query(User).filter(User.id_user == user_id).first()
    if not user:
        raise HTTPException(status_code=404, detail="User not found")
    return UserOut(
        id_user=user.id_user,
        nama_lengkap=user.nama_lengkap,
        username=user.username,
        email=user.email,
        kelamin=user.kelamin,
        nomor_telepon=user.nomor_telepon,
        tanggal_lahir=user.tanggal_lahir,
        kota_tinggal=user.kota_tinggal,
        kecamatan=user.kecamatan
    )

@app.put("/user/{user_id}", response_model=UserOut)
def update_user(user_id: int, user: UserUpdate):
    db = SessionLocal()
    db_user = db.query(User).filter(User.id_user == user_id).first()
    if not db_user:
        raise HTTPException(status_code=404, detail="User not found")
    for key, value in user.dict().items():
        if value is not None:
            setattr(db_user, key, value)
    db.commit()
    db.refresh(db_user)
    return UserOut(
        id_user=db_user.id_user,
        nama_lengkap=db_user.nama_lengkap,
        username=db_user.username,
        email=db_user.email,
        kelamin=db_user.kelamin,
        nomor_telepon=db_user.nomor_telepon,
        tanggal_lahir=db_user.tanggal_lahir,
        kota_tinggal=db_user.kota_tinggal,
        kecamatan=db_user.kecamatan
    )


@app.delete("/user/{user_id}", response_model=UserOut)
def delete_user(user_id: int):
    db = SessionLocal()
    db_user = db.query(User).filter(User.id_user == user_id).first()
    if not db_user:
        raise HTTPException(status_code=404, detail="User not found")
    db.delete(db_user)
    db.commit()
    return UserOut(
        id_user=db_user.id_user,
        nama_lengkap=db_user.nama_lengkap,
        username=db_user.username,
        email=db_user.email,
        kelamin=db_user.kelamin,
        nomor_telepon=db_user.nomor_telepon,
        tanggal_lahir=db_user.tanggal_lahir,
        kota_tinggal=db_user.kota_tinggal,
        kecamatan=db_user.kecamatan
    )


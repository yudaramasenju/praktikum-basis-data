# Nomor 1
Menampilkan Use Case Table Untuk Produk Digitalnya
---
- Use case user

| NO |  Use Case |
| ------ | ----- |
| 1 | Menambahkan akun baru|
| 2 | Membaca setiap informasi aplikasi yang ada |
| 3 | Mengupdate informasi pribadi/profile |
| 4 | Menghapus akun mereka jika tidak menggunakan Traveloka lagi  |
| 5 | User Bisa Pesan Tiket Pesawat   |
| 6 | User Bisa Pesan Hotel      |
| 7 | User Bisa Pesan Tiket Kereta Api |
| 8 | User Bisa Pesan Tiket Bus |
| 9 | User Bisa Mendapatkan Promo/diskon Terhadap pemesanan tiket |
| 10 | User bisa memberikan ulasan |
| 11 | User bisa memberikan rating |
| 12 | User bisa membatalkan pesanan |
| 13 | User melakukan pencarian tiket pesawat, kereta api, dan hotel |
| 14 | User melakukan pembayaran atas pemesanan tiket yang dipilih |
| 15 | User dapat melihat ulasan informasi t destinasi wisata tertentu |
| 16 | User dapat melihat riwayat tiket yang dipesan |
| 17 | User dapat melakukan pengaturan informasi akun |
| 18 | User dapat menghubungi layanan pelanggan untuk mendapatkan bantuan atau informasi lebih lanjut |
| 19 | User dapat mengganti jadwal pemesanan tiket tertentu |
| 20 | User dapat mengakses informasi tentang fasilitas dan layanan tambahan yang tersedia di hotel atau maskapai tertentu, seperti makanan khusus atau akses ke lounge VIP |
| 21 | User dapat mengunduh atau mencetak tiket atau dokumen penting terkait perjalanan atau penginapan yang sudah dipesan |
| 22 | User dapat melakukan perbandingan antara harga atau fasilitas yang ditawarkan oleh beberapa maskapai atau hotel berbeda, sehingga dapat memilih opsi yang paling sesuai dengan kebutuhan dan anggaran |
| 23 | User dapat Mengikuti program loyalty atau member untuk mendapatkan potongan harga atau keuntungan lain ketika melakukan pemesanan di Traveloka |
| 24 | User dapat memilih tempat duduk atau kamar hotel yang diinginkan, jika tersedia opsi untuk memilih sendiri |
| 25 | User dapat memilih tujuan destinati |
| 26 | User dapat memilih tipe kamar |
| 27 | User dapat melihat rentang harga tiket dari termurah dan termahal |
| 28 | User dapat mencari tiket konser |
| 29 | User dapat memlilih kategori tiket |
| 30 | User dapat melaukan pemesanan tiket konser |

- Use case manajemen perusahaan

| NO | Use Case |
|----|----------|
| 1  | Membuat dan mengelola akun pengguna admin |
| 2  | Membaca informasi pengguna, termasuk data pribadi dan riwayat pemesanan |
| 3  | Mengupdate informasi pengguna, seperti alamat email, nomor telepon, atau preferensi |
| 4  | Menghapus akun pengguna jika diperlukan |
| 5  | Menambahkan dan mengelola informasi tentang maskapai penerbangan, hotel, atau penyedia aktivitas wisata |
| 6  | Membaca informasi tentang maskapai penerbangan, hotel, atau aktivitas wisata, termasuk harga, ketersediaan, dan ulasan pengguna |
| 7  | Mengupdate informasi tentang maskapai penerbangan, hotel, atau aktivitas wisata, seperti harga, fasilitas, atau deskripsi |
| 8  | Menghapus informasi yang sudah tidak relevan atau tidak digunakan lagi, misalnya jika maskapai penerbangan atau hotel tidak bekerja sama lagi |
| 9  | Membuat dan mengelola promosi atau diskon untuk pemesanan tiket |
| 10 | Menganalisis data penjualan, pendapatan, dan pengguna untuk mendapatkan wawasan bisnis |
| 11 | Melihat laporan dan statistik mengenai kinerja bisnis secara keseluruhan |
| 12 | Mengelola hubungan dengan mitra bisnis, termasuk maskapai penerbangan, hotel, atau penyedia aktivitas wisata |
| 13 | Melakukan integrasi dengan sistem atau platform lain yang terkait |
| 14 | Mengelola inventori hotel, termasuk ketersediaan kamar, harga, dan promosi |
| 15 | Mengelola dan menganalisis data pelanggan, termasuk riwayat pemesanan dan preferensi pengguna |
| 16 | Mengawasi dan memantau operasional perusahaan secara real-time |
| 17 | Melihat dan menganalisis data tentang kepuasan pelanggan dan masalah yang terkait |
| 18 | Mengakses informasi tentang performa produk dan layanan, serta melihat tren pasar |
| 19 | Menggunakan alat analisis untuk membuat laporan kinerja dan proyeksi bisnis |
| 20 | Mengatur dan melacak kegiatan pemasaran, termasuk kampanye promosi dan iklan |

- Use case direksi perusahaan (dashboard, monitoring, analisis)

| NO | Use Case |
|----|----------|
| 1  | Melihat dashboard yang memberikan ringkasan kinerja perusahaan secara keseluruhan, termasuk pendapatan, laba, dan metrik kunci lainnya. |
| 2  | Memantau operasional perusahaan secara real-time, termasuk jumlah pemesanan, ketersediaan stok, dan status pengiriman. |
| 3  | Menerima peringatan atau notifikasi jika ada situasi darurat atau ketidaknormalan yang membutuhkan perhatian segera. |
| 4  | Melihat grafik dan diagram visual untuk memvisualisasikan data bisnis secara efektif. |
| 5  | Menyediakan akses cepat ke informasi penting, laporan, dan dokumen terkait. |
| 6  | Melacak kinerja tim atau departemen tertentu dalam menjalankan tugas mereka. |
| 7  | Memantau dan menganalisis tingkat kepuasan pelanggan berdasarkan umpan balik dan ulasan mereka. |
| 8  | Menganalisis data penjualan, pendapatan, dan keuntungan untuk mengidentifikasi tren dan peluang bisnis. |
| 9  | Melakukan segmentasi pelanggan berdasarkan preferensi, perilaku, dan demografi untuk menginformasikan strategi pemasaran dan penargetan. |
| 10 | Menyediakan alat prediksi dan proyeksi untuk membantu dalam perencanaan bisnis dan pengambilan keputusan jangka panjang. |
| 11 | Mengidentifikasi masalah operasional atau bottlenecks yang mempengaruhi efisiensi dan kualitas layanan. |
| 12 | Menggunakan analisis data untuk mengoptimalkan harga, promosi, dan strategi penjualan. |
| 13 | Melihat laporan dan statistik mengenai kinerja bisnis secara keseluruhan. |
| 14 | Menganalisis data pelanggan untuk mengidentifikasi peluang upselling, cross-selling, dan retensi. |
| 15 | Memantau dan mengelola risiko perusahaan, termasuk risiko keuangan, operasional, dan kepatuhan. |
| 16 | Melakukan pemantauan pasar dan analisis pesaing untuk menginformasikan strategi bisnis. |
| 17 | Mengatur dan melacak kegiatan pemasaran, termasuk kampanye promosi dan iklan. |
| 18 | Menggunakan alat analisis untuk membuat laporan kinerja dan proyeksi bisnis. |
| 19 | Mengelola hubungan dengan mitra bisnis, termasuk maskapai penerbangan, hotel, atau penyedia aktivitas wisata. |
| 20 | Melakukan integrasi dengan sistem atau platform lain yang terkait. |

# Nomor 2
Mampu mendemonstrasikan web service (CRUD) dari produk digitalnya
---
Web service ini menggunakan bahasa pemrograman ```python``` dan framework ```FastAPI```. Disini saya mencoba melakukan operasi CRUD pada tabel user.

Kita menggunakan driver ```pymysql``` untuk menghubungkan ke database MySQL. Pertama menginstall terlebih dahulu.

```py
pip install pymysql
```

Kemudian melakukan koneksi ke database.Berikut SourceCode menghubungkan ke database:

```py
from sqlalchemy import create_engine,
from sqlalchemy.orm import sessionmaker

# Buat koneksi ke database MySQL
DATABASE_URL = "mysql+pymysql://root:manchester7@localhost:3306/traveloka_database"

# Buat engine untuk koneksi ke database
engine = create_engine(DATABASE_URL)

# Buat session
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
```

- Berikut SourceCode lengkap web service CRUD [main.py](src/main.py) beserta endpoint nya.


# Berikut demonstrasi CRUD menggunakan software ```POSTMAN``` interface untuk eksplorasi web service nya.

**- CREATE/POST**

Mencoba menambahkan user baru dengan keterangan dibawah ini

![POSTUSER](Assets/POSTUSER.gif)

**- READ/GET**
 - Get All User
 
 Mencoba mendapatkan data semua user

 ![ALLUSER](Assets/GETALLUSER.gif)

 - Get ID User
 
 Mencoba mendapatkan data user berdasarkan ID

 ![IDUSER](Assets/GETIDUSER.gif)

**- UPDATE/PUT**

Mencoba melakukan perubahan data pada tabel user, mengubah isi data kolom ```kota_tinggal``` dan ```kecamatan```

![PUTUSER](Assets/PUTUSER.gif)

**- DELETE**

Mencoba melakukan penghapusan data user dengan memasukkan ID user tersebut

![DELETEUSER](Assets/DELETEUSER.gif)

# Nomor 3
Mampu mendemonstrasikan minimal 3 visualisasi data untuk business intelligence produk digitalnya
---

Dalam visualisasi ini saya menggunakan tools ``metabase`` untuk memvisualisasikan database traveloka saya. Dibawah ini terdapat beberapa visualisasi dengan bermacam grafik

- Visualisasi Jenis Kelamin User menggunakan visualisasi pie

![Jenis Kelamin](Assets/Pie_Jenis_Kelamin_User.png)

- Visualisasi Bar Jumlah Penerbangan Per Maskapai

![Penerbangan](Assets/Bar_Jumlah_Penerbangan_Per_Maskapai.png)

- Visualisasi Batang Metode Pembayaran Terpopuler/Sering Digunakan

![Metode Pembayaran](Assets/Batang_Metode_Pembayaran_Terpopuler.png)

# Nomor 4
Mampu mendemonstrasikan penggunaan minimal 3 built-in function dengan ketentuan :
---
**1. Regex**

Regex adalah singkatan dari Regular Expression, yang digunakan untuk mencocokkan dan memanipulasi pola teks. Dalam contoh ini, kita akan menggunakan fungsi REGEXP_REPLACE dalam MySQL untuk mengganti pola teks dalam sebuah string.
```sql
SELECT REGEXP_REPLACE(nama_po, '[aeiou]', '*') AS Tiket_Bus
from tiket_bus;
```
Hasilnya seperti yang terlihat dalam gambar, huruf vokal ```aiueo``` diganti dengan ```asterisk *```

**``` Result ```**


![Result Regex](Assets/Regex.png)
---

**2. Substring**

Fungsi SUBSTRING digunakan untuk mengambil sebagian teks dari sebuah string. Dalam contoh ini, kita akan menggunakan fungsi SUBSTRING dalam MySQL untuk mengambil Lima karakter pertama dari sebuah string.

```sql
SELECT SUBSTRING(nama_maskapai, 1, 5) AS Nama_Maskapai
from tiket_pesawat;
```

**``` Result ```**

![Result Substring](Assets/Substring.png)

**3. Upper dan Lower**

Fungsi UPPER dan LOWER digunakan untuk mengubah teks menjadi huruf kapital (uppercase) atau huruf kecil (lowercase). Dalam contoh ini, kita akan menggunakan fungsi UPPER dan LOWER dalam MySQL untuk mengubah teks menjadi huruf kapital atau huruf kecil.

```sql
SELECT UPPER(nama_kereta) AS UpperResult, LOWER(tujuan) AS LowerResult
FROM tiket_kereta_api;
```

**``` Result ```**

![Upper Lower Result](Assets/Upper_Lower.png)

# Nomor 5
Mampu mendemonstrasikan dan menjelaskan penggunaan Subquery pada produk digitalnya
---

Subquery adalah sebuah query yang ditempatkan di dalam query utama dan digunakan untuk menghasilkan data yang akan digunakan dalam pengolahan lebih lanjut oleh query utama.

- Pemfilteran Data

Misalkan kita ingin mengambil daftar Tiket Bus dengan harga di atas rata-rata harga dari semua tiket yang ada. Kita dapat menggunakan subquery untuk mencari nilai rata-rata harga terlebih dahulu dan kemudian memfilter produk berdasarkan harga tersebut.


```sql
SELECT nama_po, harga
FROM tiket_bus
WHERE harga > (SELECT AVG(harga) FROM tiket_bus);
```

Subquery ``` (SELECT AVG(harga) FROM tiket_bus)``` digunakan untuk menghitung rata-rata harga dari semua produk. Kemudian, hasil subquery ini digunakan dalam query utama untuk memfilter produk dengan harga di atas rata-rata.

``` Result ```

![Subquery 1](Assets/Subquery1.png)

- Menghubungkan Data Antar Tabel

Misalkan kita memiliki dua tabel, yaitu "user" dan "booking". Tabel "user" berisi informasi tentang pengguna yang melakukan pemesanan, sementara tabel "booking" berisi informasi tentang pemesanan yang dilakukan oleh pengguna. Kita ingin mencari pengguna yang memiliki lebih dari satu pemesanan.

```sql
SELECT id_user, nama_lengkap, email
FROM user
WHERE id_user IN (
  SELECT id_user
  FROM booking
  GROUP BY id_user
  HAVING COUNT(*) > 1
);
```

Subquery digunakan untuk mengambil id_user dari tabel "booking" yang memiliki lebih dari satu pemesanan. Pernyataan SELECT utama menggunakan IN untuk memfilter pengguna berdasarkan id_user yang dihasilkan oleh subquery

``` Result ```

![Subquery 2](Assets/user.png)

# Nomor 6
Mampu mendemonstrasikan dan menjelaskan penggunaan Transaction pada produk digitalnya
---

Transaction (transaksi) dalam MySQL adalah kumpulan pernyataan SQL yang dijalankan sebagai satu kesatuan yang harus dilakukan secara lengkap atau dibatalkan sepenuhnya jika terjadi kesalahan atau kegagalan. Transaksi memungkinkan Anda untuk melakukan operasi yang melibatkan beberapa pernyataan SQL dan memastikan konsistensi data dalam database.

```sql
START TRANSACTION;

-- Langkah-langkah dalam transaksi
UPDATE user SET nama_lengkap = 'Marcus Rashford' WHERE id_user = 23;
INSERT INTO review (nama_pengguna,komentar,rating) VALUES ('Marcus Rashford','Bagus Sekali',9);
DELETE from tiket_kereta_api where id_tiket_kereta_api = 21;
-- Jika semua langkah berhasil, commit transaksi
COMMIT;
```
# Nomor 7
Mampu mendemonstrasikan dan menjelaskan penggunaan Procedure / Function dan Trigger pada produk digitalnya
---

- Procedure

Procedure adalah kumpulan perintah SQL yang telah diberi nama dan dapat dipanggil secara terpisah. Procedure dapat menerima argumen sebagai input dan mengembalikan output jika diperlukan.

```sql
DELIMITER $$
CREATE PROCEDURE HitungJumlahTiket(IN user_id INT, OUT jumlah_tiket INT)
BEGIN
    SELECT COUNT(*) INTO jumlah_tiket FROM booking WHERE id_user = user_id;
END$$
DELIMITER ;
```

Dalam contoh ini, procedure ``HitungJumlahTiket`` menerima parameter ``user_id`` sebagai input dan mengembalikan jumlah tiket yang dipesan oleh pengguna dengan ``id_user`` yang sesuai ke dalam variabel ``jumlah_tiket``.

Untuk memanggil procedure tersebut,Kita dapat menggunakan perintah berikut:

```sql
CALL HitungJumlahTiket(17, @jumlah_tiket);
SELECT @jumlah_tiket AS JumlahTiket;
```

Perintah di atas akan memanggil procedure ``HitungJumlahTiket`` dengan misalkan user_id = 17 dan menyimpan hasilnya dalam variabel ``@jumlah_tiket``. Kemudian, perintah ``SELECT`` digunakan untuk menampilkan nilai dari ``@jumlah_tiket`` sebagai output.

`` Result ``

![Procedure](Assets/Procedure.png)

- Function

Function adalah kumpulan perintah SQL yang memiliki nama dan dapat mengembalikan nilai.Function dapat menerima argumen sebagai input dan mengembalikan nilai hasil perhitungan atau transformasi data.

```sql
DELIMITER $$
CREATE FUNCTION HitungJumlahTiket(jenis VARCHAR(255)) RETURNS INT
deterministic
no sql
BEGIN
    DECLARE total INT;
    SELECT SUM(qty) INTO total
    FROM booking
    WHERE jenis_tiket = jenis;
    RETURN total;
END$$
DELIMITER ;
```

Dalam contoh di atas, kita membuat sebuah function dengan nama ``HitungJumlahTiket`` yang menerima parameter jenis berupa string. Function ini akan menghitung jumlah tiket yang sudah dipesan berdasarkan jenis tiket yang diberikan.

Untuk menggunakan function tersebut, Anda dapat melakukan query sebagai berikut:

```sql
SELECT HitungJumlahTiket('Pesawat') AS total_tiket_pesawat,
       HitungJumlahTiket('Hotel') AS total_tiket_hotel,
       HitungJumlahTiket('Kereta Api') AS total_tiket_kereta,
       HitungJumlahTiket('Bus') AS total_tiket_bus;
```

``` Result ```

![Function](Assets/Function.png)
- Trigger

Trigger adalah tindakan otomatis yang dipicu oleh perubahan data pada tabel tertentu.

```sql
DELIMITER $$
CREATE TRIGGER update_booking_status
AFTER INSERT ON transaksi
FOR EACH ROW
BEGIN
    DECLARE total_pembayaran DECIMAL(10, 2);
    SET total_pembayaran = (SELECT SUM(jumlah_pembayaran) FROM transaksi WHERE id_booking = NEW.id_booking);
    
    IF total_pembayaran != 0 THEN
        UPDATE booking
        SET status = 2
        WHERE id_booking = NEW.id_booking;
    ELSE
        UPDATE booking
        SET status = 1
        WHERE id_booking = NEW.id_booking;
    END IF;
END$$
DELIMITER ;
```

Dalam trigger ini, setiap kali ada data baru yang dimasukkan ke tabel transaksi, trigger ini akan diaktifkan. Trigger akan menghitung total pembayaran untuk id_booking yang baru ditambahkan,jika total pembayaran != 0 , maka status_pembayaran di tabel booking akan diubah menjadi 2 = Confirmed. Jika tidak, maka status_pembayaran akan diubah menjadi 1 = Pending.

# Nomor 8
Mampu mendemonstrasikan Data Control Language (DCL) pada produk digitalnya
---

DCL digunakan untuk mengendalikan akses pengguna ke objek database dan untuk memberikan izin dan pembatasan yang sesuai.

**- GRANT**

Perintah GRANT digunakan untuk memberikan izin akses kepada pengguna atau peran di database.

```sql
GRANT SELECT, INSERT ON tiket_pesawat TO user1@localhost;
```

Perintah di atas memberikan izin SELECT dan INSERT pada tabel "tiket_pesawat" kepada pengguna "user1".

**- REVOKE**

Digunakan untuk mencabut atau menghapus izin akses yang sebelumnya diberikan kepada pengguna atau peran di database.

```sql
REVOKE SELECT ON tiket_pesawat FROM user1@localhost;
```

Perintah di atas mencabut izin SELECT yang sebelumnya diberikan kepada pengguna "user1" pada tabel "flights"

**- Create User**

Digunakan untuk membuat pengguna baru dalam database.

```sql
CREATE USER 'user1'@'localhost' IDENTIFIED BY 'password';
```

Perintah di atas membuat pengguna baru dengan nama "user1" dan kata sandi "password" pada host "localhost".

**- Drop User**

Digunakan untuk menghapus pengguna dari database.

```sql
DROP USER 'user1'@'localhost';
```

Perintah di atas menghapus pengguna "user1" dari host "localhost".

# Nomor 9
Mampu mendemonstrasikan dan menjelaskan constraint yang digunakan pada produk digitalnya:
---

- Foreign Key


Foreign key digunakan untuk membuat hubungan antara dua tabel. Ini memastikan integritas referensial antara tabel tersebut.

```sql
CREATE TABLE `booking_bus` (
  `id_booking` int NOT NULL AUTO_INCREMENT,
  `id_user` int DEFAULT NULL,
  `id_tiket_bus` int DEFAULT NULL,
  `total_harga` double DEFAULT NULL,
  `status` int DEFAULT NULL,
  PRIMARY KEY (`id_booking`),
  KEY `bbus_id` (`id_user`),
  KEY `bbus_tiket` (`id_tiket_bus`),
  CONSTRAINT `bbus_id` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`),
  CONSTRAINT `bbus_tiket` FOREIGN KEY (`id_tiket_bus`) REFERENCES `tiket_bus` (`id_tiket_bus`)
);

CREATE TABLE `tiket_bus` (
  `id_tiket_bus` int NOT NULL AUTO_INCREMENT,
  `nama_po` varchar(255) DEFAULT NULL,
  `kelas` varchar(255) DEFAULT NULL,
  `harga` int DEFAULT NULL,
  `asal` varchar(255) DEFAULT NULL,
  `tujuan` varchar(255) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  PRIMARY KEY (`id_tiket_bus`)
);
```

- Index

Indeks digunakan untuk meningkatkan kinerja pencarian dan pengurutan data dalam tabel. Ini memungkinkan akses data yang lebih cepat dengan membuat struktur data tambahan yang mengindeks kolom atau kombinasi kolom tertentu.

```sql
CREATE TABLE `tiket_pesawat` (
  `id_tiket_pesawat` int NOT NULL AUTO_INCREMENT,
  `nomor_penerbangan` varchar(255) DEFAULT NULL,
  `nama_maskapai` varchar(255) DEFAULT NULL,
  ...
  PRIMARY KEY (`id_tiket_pesawat`),
  KEY `fk_tiket_pesawat_maskapai` (`id_maskapai`),
  KEY `idx_nama_maskapai` (`nama_maskapai`),
  CONSTRAINT `fk_tiket_pesawat_maskapai` FOREIGN KEY (`id_maskapai`) REFERENCES `maskapai` (`id_maskapai`)
)
```

- Unique Key

Unique key memastikan bahwa nilai-nilai dalam kolom tertentu harus unik, yaitu tidak boleh ada duplikat. Ini membantu menjaga integritas data dengan mencegah duplikasi nilai yang tidak diinginkan.


```sql
CREATE TABLE `user` (
  `id_user` int NOT NULL AUTO_INCREMENT,
  `nama_lengkap` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `kelamin` enum('L','P') DEFAULT NULL,
  `nomor_telepon` varchar(255) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `kota_tinggal` varchar(255) DEFAULT NULL,
  `kecamatan` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_user`),
  UNIQUE KEY `email_unique` (`email`)
)
```

# Nomor 10
Mendemonstrasikan produk digitalnya kepada publik dengan cara-cara kreatif melalui video Youtube
---

[Youtube](https://youtu.be/9Er_GpwejXs)

# Nomor 11
Mendemonstrasikan UI untuk CRUDnya
---

Disini saya mencoba mendemonstrasikan UI dengan menggunakan ```Java Swing```. Jadi database traveloka yang saya buat terhubung dengan program java. Di bawah ini saya mendemonstrasikan ```Login (Read)``` dan ```Register (Create)``` User pada program Traveloka java berbasis UI.

- Login

![Login](Assets/LOGIN.gif)

- Register

![Register](Assets/REGISTRASI.gif)

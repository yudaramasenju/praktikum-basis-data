# CREATE/INSERT
``` USER ```
<pre><code>INSERT INTO user (nama_lengkap,nama_pengguna,nomor_telepon,kelamin,tanggal_lahir,kota_tinggal,alamat,email,password,akun_terhubung)
VALUES ('Yuda Ristian Asgari','yudaristian22',0881023764187,'L',20030322,'Bandung','Rancaekek','yudaramasenju@gmail.com','yuda123','Google'),
('Siti Arifah Sumiyati','sitiarifah',088657340846,'p',20030211,'SUmedang','Jatinangor','sitiarifah@gmail.com','sitiarifah123','Google'),
('Sopian Renaldi','boopablo',0887313620967,'L',20020910,'Bandung','Rancaekek','sopianrenaldi@gmail.com','sopian123','Facebook'),
('Ridwan Ahmad Fauzan','ridwanahmad',088408172898,'L',20030510,'Subang','Cibiru','ridwanahmad@gmail.com','ridwan123','Google'),
('Helmi Luthfi Januar','gatau123',088336534217,'L',20030102,'Bandung','Rancaekek','helmiluthfi@gmail.com','helmi123','Facebook'),
('Agus Aji Pangestu','agusajialpra',088591004758,'L',20020810,'Bandung','Cibiru','agusaji@gmail.com','agusaji123','Facebook'),
('Ilmi Septyasella','cutiespetit',0884791709142,'P',20020908,'Sumedang','Parakan Muncang','ilmiseptya@gmail.com','ilmi123','Facebook'),
('Rahma Aulia Febrianti','rahmaaulf',0882806236738,'P',20030209,'Bandung','Cileunyi','rahmaaulia@gmail.com','rahma123','Google'),
('Muhammad Zaky','muhammadzaky',088489489859,'L',20030125,'Bandung','Banjaran','zakyms@gmail.com','zaky123','Google'),
('Sami Irhamnillah','samiirham',0886388461215,'L',20030203,'Indramayu','Buah Batu','samiirham@gmail.com','sami123','Google'),
('Bayu Sapta Syawali','by.u',08867585857,'L',20020915,'Bandung','Rancaekek','bayusapta@gmail.com','bayu123','Google'),
('Sri Purnama Dewi','sriprnmdw',08816964565,'P',20040505,'Bandung','Cileunyi','sripurnama@gmail.com','sri123','Facebook'),
('Alissa Qotrunnada','alissaq',0881343906230,'P',20031202,'Bandung','Rancaekek','alissaq@gmail.com','alissa123','Google'),
('Wiki Nurrohman','wikinur',088639674585,'L',20011125,'Subang','Kiara Condong','wikinur@gmail.com','wiki123','Google'),
('Muhammad Syamil','syamil',088281874287,'L',20031012,'Ciamis','Cibiru','msyamil@gmail.com','syamil123','Google'),
('Raihan Firdaus','daus','0881023766198','L','20021201','Bandung','Cipadung','raihanfirdaus@gmail.com','raihan123','Facebook'),
('Tifani Apriliani','tifaaa','088102789382','P','20020302','Subang','Cibiru','tifaniapriliani@gmail.com','tifani123','Facebook'),
('Gisva Rapa Pradila','agis','0881022892189','P','20021231','Bandung','Rancaekek','gisvarapa@gmail.com','gisva123','Facebook'),
('Mutiara Annisa','utii','0882098645178','P','20030409','Bandung','Cileunyi','mutiara@gmail.com','mutiara123','Google'),
('Zaskiya','jaskeu','0889208901928','P','20030910','Sumedang','Jatinangor','zaskiya@gmail.com','zaskiya123','Google')
</pre></code>

``` TIKET PESAWAT ```
<pre><code>insert into maskapai (nama_maskapai, kode_IATA, jenis_pesawat, tujuan)
values ('Air Asia','QZ','Boeing 737','Surabaya'),
('Batik Air','ID','Boeing 737','Surabaya'),
('Citilink','QG','Boeing 737','Surabaya'),
('Garuda Indonesia','GA','Boeing 737','Surabaya'),
('Lion Air','JT','Boeing 737','Surabaya'),
('SuperAirJet','SJ','Boeing 737','Surabaya'),
('Wings Air','IW','Boeing 737','Surabaya'),
('Air Asia','QZ','Boeing 747','Surabaya'),
('Batik Air','ID','Boeing 747','Surabaya'),
('Citilink','QG','Boeing 747','Surabaya'),
('Garuda Indonesia','GA','Boeing 747','Surabaya'),
('Lion Air','JT','Boeing 747','Surabaya'),
('SuperAirJet','SJ','Boeing 747','Surabaya'),
('Wings Air','IW','Boeing 747','Surabaya'),
('Air Asia','QZ','Boeing 737','Yogyakarta'),
('Batik Air','ID','Boeing 737','Yogyakarta'),
('Citilink','QG','Boeing 737','Yogyakarta'),
('Garuda Indonesia','GA','Boeing 737','Yogyakarta'),
('Lion Air','JT','Boeing 737','Yogyakarta'),
('SuperAirJet','SJ','Boeing 737','Yogyakarta'),
('Wings Air','IW','Boeing 737','Yogyakarta'),
('Air Asia','QZ','Boeing 747','Yogyakarta'),
('Batik Air','ID','Boeing 747','Yogyakarta'),
('Citilink','QG','Boeing 747','Yogyakarta'),
('Garuda Indonesia','GA','Boeing 747','Yogyakarta'),
('Lion Air','JT','Boeing 747','Yogyakarta'),
('SuperAirJet','SJ','Boeing 747','Yogyakarta'),
('Wings Air','IW','Boeing 747','Yogyakarta')
</pre></code>

``` HOTEl ```
<pre><code>insert into hotel(nama_hotel, lokasi, fasilitas,harga,rating)
values('Grand Tjokro Premiere Bandung', 'Bandung','AC + Restoran + Kolam renang + Parkir + Lift + Wifi', 584100,8.5),
('The Alana Yogyakarta Hotel & Convention Center', 'Yogyakarta','AC + Restoran + Kolam renang + Parkir + Lift + Wifi', 710000,8.8),
('Melia Purosani Yogyakarta', 'Yogyakarta','AC + Restoran + Kolam renang + Parkir + Lift + Wifi', 1350000,8.7),
('ARTOTEL Suites Mangkuluhur Jakarta', 'Jakarta','AC + Restoran + Kolam renang + Parkir + Lift + Wifi', 963000,8.4),
('Hotel Indonesia Kempinski Jakarta', 'Jakarta','AC + Restoran + Kolam renang + Parkir + Lift + Wifi', 3600100,8.9),
('Grand Darmo Suite by AMITHYA', 'Surabaya','AC + Restoran + Kolam renang + Parkir + Lift + Wifi', 400000,8.3),
('The Apurva Kempinski Bali', 'Bali','AC + Restoran + Kolam renang + Parkir + Lift + Wifi', 5600000,8.9),
('The Anvaya Beach Resort Bali', 'Bali','AC + Restoran + Kolam renang + Parkir + Lift + Wifi', 2900000,8.9),
('Lorin Sentul Hotel', 'Bogor','AC + Restoran + Kolam renang + Parkir + Lift + Wifi', 427000,7.9),
('Salak Hevea Syariah', 'Bogor','AC + Restoran + Kolam renang + Parkir + Lift + Wifi', 300000,8.4)
</pre></code>

# READ
<pre><code>READ
SELECT * FROM USER;
SELECT * FROM USER WHERE kelamin = 'L';
</pre></code>

# UPDATE
<pre><code>UPDATE
UPDATE user
SET nama_pengguna = 'fani'
WHERE id_user = 17;
</pre></code>

# DELETE
<pre><code>DELETE
DELETE FROM user
WHERE id_user = 19;
</pre></code>
